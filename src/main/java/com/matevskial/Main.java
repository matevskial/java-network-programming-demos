package com.matevskial;

import com.matevskial.demoframework.DemoMenu;
import com.matevskial.demos.http.HttpDemo;
import com.matevskial.demos.http.SimpleHttpServer;
import com.matevskial.demos.ipaddresses.IPAddresses;
import com.matevskial.demos.multithreading.MultithreadingDemo;
import com.matevskial.demos.udp.EchoServerAndClientDemo;
import com.matevskial.demos.tcp.DayTimeServerAndClientDemo;

/**
 * Main entry of program
 * Contains menu with entries to choose from for starting a demo
 */
public class Main {

    private static final String HEADER_TEXT = "Choose a demo to run using entry numbers.\n" +
            "Type \"quit\" without quotes to exit the program.\n" +
            "Type \"back\" without quotes to back to the previous menu.";

    public static void main(String[] args) {

        System.out.println("Java network programming demos");
        DemoMenu m = new DemoMenu(HEADER_TEXT);
        m.addEntry("INetAddress demo", new IPAddresses());
        m.addEntry("Udp Echo Server and client demo", new EchoServerAndClientDemo());
        m.addEntry("Tcp DayTime Server and client demo", new DayTimeServerAndClientDemo());
        m.addEntry("Multithreading demo", new MultithreadingDemo());
        m.addEntry("HTTP demo", new HttpDemo());
        m.display();
    }
}
