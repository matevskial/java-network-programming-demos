package com.matevskial.demos.multithreading;

import com.matevskial.demoframework.IDemo;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A class to demonstrate producer/consumer pattern implemented with wait/notify
 *
 */
public class ProducerConsumer implements IDemo {

    private static final Queue<Integer> q = new ConcurrentLinkedQueue<>();

    private static class Consumer implements Runnable {

        @Override
        public void run() {
            synchronized (q) {
                try {
                    // the while loop is only usable when the wait is timed
                    // (since after the timeout the thread will continue to execute)
                    // or start the waiting conditionally when a new thread executes this line of code for the first time
                    while (q.isEmpty()) {
                        System.out.printf("Thread %s is waiting%n", Thread.currentThread().getName());
                        q.wait();
                    }
                } catch (InterruptedException e) {
                    System.out.printf("Thread %s interrupted from sleeping%n", Thread.currentThread().getName());
                }

                try {
                    Integer res = q.poll();
                    System.out.printf("Thread %s polls the integer %s%n", Thread.currentThread().getName(), res);
                } catch (Exception e) {
                    System.out.printf("Thread %s tried to poll from an empty queue.%n",
                            Thread.currentThread().getName());
                }
            }

        }

    }

    private static class Producer implements Runnable {

        @Override
        public void run() {
            System.out.printf("Producer thread %s, puts value into queue%n", Thread.currentThread().getName());
            q.add(5);
            synchronized (q) {
                q.notifyAll();
            }
        }

    }

    public void run() {
        Thread c1 = new Thread(new Consumer(), "consumer-1");
        Thread c2 = new Thread(new Consumer(), "consumer-2");

        Thread p1 = new Thread(new Producer(), "producer-1");
        Thread p2 = new Thread(new Producer(), "producer-2");

        p1.start();
        p2.start();
        c1.start();
        c2.start();
        try {
            p1.join();
            p2.join();
            c1.join();
            c2.join();
        } catch (InterruptedException e) {
            System.out.printf("Thread %s, interrupted while waiting for other threads to finish\n",
                    Thread.currentThread().getName());
        }
    }
}
