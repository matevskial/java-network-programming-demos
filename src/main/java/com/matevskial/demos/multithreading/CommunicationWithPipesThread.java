package com.matevskial.demos.multithreading;

import java.io.PipedOutputStream;
import java.io.PrintStream;

/**
 * A class to demonstrate inter-thread communication with piped streams
 */
public class CommunicationWithPipesThread implements Runnable {
    private final PipedOutputStream outputStream;

    public CommunicationWithPipesThread(PipedOutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    public void run() {
        PrintStream printOutputStream = new PrintStream(outputStream);
        printOutputStream.printf("data-%s", Thread.currentThread().getName());
        printOutputStream.close();
    }
}
