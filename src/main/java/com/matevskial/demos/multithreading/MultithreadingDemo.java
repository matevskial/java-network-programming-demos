package com.matevskial.demos.multithreading;

import com.matevskial.demoframework.DemoMenu;
import com.matevskial.demoframework.IDemo;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Scanner;

/**
 * A class that contains some multithreading demos
 *
 * Notes about multithreading
 * * a daemon threads won't be able to finish its work if there is no user thread active
 *
 * * atomic operation group operations that will execute in one go
 * (no interleaving with other threads while atomic operation executes)
 *   * reference assignments and primitive assignments(except long and double) are atomic operations
 *   * assignment to references and primitives declared with volatile
 *
 * * calling wait on synchronized objet will release the lock the current thread got for that object,
 * and will wait for other thread to execute notify on that same object
 *
 * * With instance of ThreadGroup
 *   * can set max priority for threads in group
 *   * can set isDaemon flag for threads in group
 *   * can check the number of active threads and sub-groups
 */
public class MultithreadingDemo implements IDemo {

    private static final String HEADER_TEXT = "Java multithreading demo";

    private static final Object resource1 = new Object();
    private static final Object resource2 = new Object();

    @Override
    public void run() {
        DemoMenu m = new DemoMenu(HEADER_TEXT);
        m.addEntry("Deadlock demo", this::deadlock);
        m.addEntry("wait/notify example to demonstrate producer/consumer pattern", new ProducerConsumer());
        m.addEntry("Inter-thread communication using piped streams", this::pipedStreams);
        m.addEntry("ThreadGroup demo", this::threadGroup);
        m.display();
    }

    private void threadGroup() {
        ThreadGroup threadGroup = new ThreadGroup("exampleThreadGroup");
        Thread t1 = new Thread(threadGroup, this::threadInThreadGroup, "exampleThread-1");
        Thread t2 = new Thread(threadGroup, this::threadInThreadGroup, "examplerThread-2");
        threadGroup.setMaxPriority(Thread.NORM_PRIORITY);

        t1.start();
        t2.start();
        threadGroup.list();
    }

    private void threadInThreadGroup() {
        Thread t = Thread.currentThread();
        System.out.printf("Thread %s in a thread group %s executing%n", t.getName(), t.getThreadGroup().getName());
    }

    private void pipedStreams() {
        PipedOutputStream outputStream = new PipedOutputStream();

        Thread t = new Thread(new CommunicationWithPipesThread(outputStream), "pipeThread");
        t.start();
        try {
            PipedInputStream inputStream = new PipedInputStream(outputStream);
            Scanner in = new Scanner(inputStream);
            System.out.println("Reading data from piped stream");
            String data = in.next();
            System.out.printf("Data got from a running thread using piped streams: %s%n", data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deadlock() {
        Thread startDeadLock = new Thread(this::startDeadLock, "startDeadlockThread");
        startDeadLock.start();
        try {
            System.out.println("Sleep current thread to wait for the two deadlock threads to start.");
            Thread.sleep(1100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Press any key to exit the deadlock state.");
        try {
            System.in.read();
            startDeadLock.interrupt();
            Thread.sleep(100);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void startDeadLock() {
        Thread t1 = new Thread(() -> {
            System.out.printf("Thread %s trying to lock resource1%n", Thread.currentThread().getName());
            synchronized (resource1) {
                System.out.printf("Thread %s locked resource1%n", Thread.currentThread().getName());
                try {
                    // Give time for the other thread to start, so we achieve deadlock
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.printf("Thread %s trying to lock resource2%n", Thread.currentThread().getName());
                synchronized (resource2) {
                    System.out.printf("Thread %s locked resource2%n", Thread.currentThread().getName());
                }
            }
        }, "deadlockThread-1");

        Thread t2 = new Thread(() -> {
            System.out.printf("Thread %s trying to lock resource2%n", Thread.currentThread().getName());
            synchronized (resource2) {
                System.out.printf("Thread %s locked resource2%n", Thread.currentThread().getName());
                try {
                    // Give time for the other thread to start, so we achieve deadlock
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.printf("Thread %s trying to lock resource1%n", Thread.currentThread().getName());
                synchronized (resource1) {
                    System.out.printf("Thread %s locked resource1%n", Thread.currentThread().getName());
                }
            }
        }, "deadlockThread-2");

        t1.start();
        t2.start();
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            System.out.println("Exiting deadlock state");
        }
    }
}
