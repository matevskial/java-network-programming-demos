package com.matevskial.demos.http;

import java.io.IOException;

import com.matevskial.demoframework.DemoMenu;
import com.matevskial.demoframework.IDemo;

/**
 * Starts a menu to select demos related to HTTP server
 *
 */
public class HttpDemo implements IDemo {

    private String HEADER_TEXT = "Java HTTP";

    @Override
    public void run() {
        DemoMenu m = new DemoMenu(HEADER_TEXT);
        m.addEntry("Start simple HTTP server", this::startHttpServer);
        m.display();
    }

    private void startHttpServer() {
        Thread httpServerStarterThread = new Thread(() -> {
        	SimpleHttpServer server = new SimpleHttpServer(6999);
        	Thread httpServer = new Thread(server, "httpServer");
        	httpServer.start();
        	
        	try {
        		httpServer.join();
        	} catch (InterruptedException e) {
				server.closeServerSocket();
			}
        	
        }, "httpServerStarter");
        
        httpServerStarterThread.start();
        System.out.println("Press any key to exit the simple http server.");
        try {
        	System.in.read();
            httpServerStarterThread.interrupt();
            Thread.sleep(100);
        } catch (InterruptedException e) {
            System.out.printf("Interrupted while sleeping after the thread %s was interrupted- %s%n", httpServerStarterThread.getName(), e);
        } catch (IOException e) {
			System.out.printf("Unable to read user input that interrups the simple http server thread - %s%n", e);
		}
    }
}
