package com.matevskial.demos.http;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Instances of this class represent simple HTTP server
 */
public class SimpleHttpServer implements Runnable {

	private static final String SERVER_NAME = "SimpleHttpServer";
	
	private static final String HTTP_NEW_LINE = "\r\n";

	private static final Map<Integer, String> statusCodes = Map.of(200, "200 OK", 400, "400 BAD REQUEST", 404,
			"404 NOT FOUND", 405, "405 BAD METHOD", 501, "NOT IMPLEMENTED", 505, "505 UNSUPPORTED HTTP VERSION");

	private static final Set<String> supportedHttpVersions = Set.of("HTTP/1.0", "HTTP/1.1");
	
	private static final Set<String> supportedHttpMethods = Set.of("GET");

	private static String root = SimpleHttpServer.class.getClassLoader().getResource("www").getFile();

	private final int port;
	
	private ServerSocket httpServerSocket;
	private boolean isServerClosed;

	public SimpleHttpServer(int port) {
		this.port = port;
		isServerClosed = false;
	}

	@Override
	public void run() {
		System.out.printf("Starting the server at port %s%n", port);

		try {
			httpServerSocket = new ServerSocket(port);
			httpServerSocket.setSoTimeout(500);
		} catch (IOException e) {
			System.out.printf("Unable to start the server - %s%n", e);
			return;
		}

		while (!isServerClosed) {
			try {
				Socket requestSocket = httpServerSocket.accept();
				startHandlerThread(requestSocket);
			} catch (SocketTimeoutException e) {
				// ignored exception, if a thread calls closeServerSocket, 
				// the code will exit this while loop
			} catch (IOException e) {
				System.out.printf("Unable to accept incoming requests - %s%n", e);
			}
		}
		
		try {		
			httpServerSocket.close();
		} catch (Exception e) {
			System.out.printf("Unable to close the server socket - not opened or already closed - %s%n", e);
			return;
		}
		
		System.out.println("Simple http server closed.");
	}
	
	public void closeServerSocket() {
		System.out.println("Closing simple http server gracefully.");
		isServerClosed = true;
	}

	private void startHandlerThread(Socket requestSocket) {
		try {
			Thread requestThread = new Thread(new Handler(requestSocket));
			requestThread.start();
		} catch (IOException e) {
			System.out.printf("Unable to process the request - %s%n", e);
		}

	}

	/**
	 * Represents a handler for each HTTP request
	 * It's run method will be executed by separate thread
	 *
	 */
	private static class Handler implements Runnable {
		private final Socket requestSocket;
		private BufferedReader br;
		private BufferedOutputStream bos;
		private PrintWriter pw;

		private Request parsedRequest;

		public Handler(Socket requestSocket) throws IOException {
			this.requestSocket = requestSocket;
		}

		@Override
		public void run() {
			try {
				createAndOpenStreams();
				readRequestData();
				doResponse();
				closeConnection();
			} catch(IOException e) {
				System.out.printf("Unable to process the request - %s%n", e);
			}
			
		}

		private void createAndOpenStreams() throws IOException {
			br = new BufferedReader(new InputStreamReader(requestSocket.getInputStream()));
			bos = new BufferedOutputStream(requestSocket.getOutputStream());
			pw = new PrintWriter(
					new BufferedWriter(new OutputStreamWriter(requestSocket.getOutputStream())));
		}

		private void closeConnection() throws IOException {
			bos.close();
			pw.close();
			requestSocket.close();
		}

		private void doResponse() throws IOException {
			// even number of elements
			// first element header, second element value, third element header, fourth
			// element value and so on
			List<String> headers = new ArrayList<>();
			headers.add("Server");
			headers.add(SERVER_NAME);
			
			if(!supportedHttpVersions.contains(parsedRequest.httpVersion)) {
				respondStatusCode(505);
				respondHeaders(headers);
				respondNewLine();
			}
			
			if(!supportedHttpMethods.contains(parsedRequest.method)) {
				respondStatusCode(501);
				respondHeaders(headers);
				respondNewLine();
			}
			
			File file = new File(parsedRequest.path);
			if (!file.exists()) {
				respondStatusCode(404);
				respondHeaders(headers);
				respondNewLine();
			} else if (file.isFile()) {
				respondFile(file, headers);
			} else {
				respondFolder(file, headers);
			}
			
		}

		private void respondFolder(File file, List<String> headers) {
			String[] fileNames = file.list();
			
			String sortOrder = getSortOrder();
			sortFileNames(fileNames, sortOrder);
			
			
			headers.add("Content-type");
			headers.add("text/html");

			respondStatusCode(200);
			respondHeaders(headers);
			respondNewLine();

			pw.println("<html>");
			pw.println("<body>");
			pw.println("<table>");
			pw.println("<thead><tr><th>Name</th></thead>");
			pw.println("<tbody>");

			for (String name : fileNames) {
				pw.printf("<tr><td>%s</td></tr>", name);
			}

			pw.println("</tbody>");
			pw.println("</table>");
			pw.println("</body>");
			pw.println("</html>");

			pw.flush();
			
		}

		private void sortFileNames(String[] fileNames, String sortOrder) {
			if(sortOrder != null && (sortOrder.equals("asc") || sortOrder.equals("desc"))) {
				Arrays.sort(fileNames, (a, b) -> sortOrder.equals("asc") ? 
						a.compareTo(b) : b.compareTo(a)
				);
			}
		}

		private String getSortOrder() {
			String[] param = new String[] {"sort", "asc"};
			if(parsedRequest.method.equals("GET")) {
				if(!parsedRequest.queryParams.isEmpty()) {
					param = parsedRequest.queryParams.get(0);
				} 
			} else if(parsedRequest.method.equals("POST")) {
				if(!parsedRequest.data.isEmpty()) {
					param = parsedRequest.data.get(0).split("=");
				}
			}
			
			if(param[0].startsWith("sort") && param.length >= 1) {
				return param[1].toLowerCase();
			}
			
			return null;
		}

		private void respondFile(File file, List<String> headers) throws IOException {
			try (BufferedInputStream fileIn = new BufferedInputStream(new FileInputStream(file))) {
				long fileSize = Files.size(file.toPath());
				String contentType = Files.probeContentType(file.toPath());
				headers.add("Content-type");
				headers.add(contentType);

				headers.add("Content-length");
				headers.add(Long.toString(fileSize));

				respondStatusCode(200);
				respondHeaders(headers);
				respondNewLine();
				respondData(fileIn);
			}
			
		}

		private void respondNewLine() {
			pw.print("\r\n");
			pw.flush();
		}

		private void respondStatusCode(int statusCode) {
			pw.printf("%s %s", parsedRequest.httpVersion, statusCodes.get(statusCode));
			respondNewLine();
			pw.flush();
		}

		private void respondHeaders(List<String> headers) {
			for (int i = 0; i < headers.size(); i += 2) {
				pw.printf("%s: %s", headers.get(i), headers.get(i + 1));
				pw.println();
			}

			pw.flush();
		}

		private void respondData(BufferedInputStream fileIn) throws IOException {
			int bufferSize = 2 * 2048;
			byte[] bufferData = new byte[bufferSize];
			int bytesRead = fileIn.read(bufferData, 0, bufferSize);

			while (bytesRead != -1) {
				bos.write(bufferData, 0, bytesRead);
				bytesRead = fileIn.read(bufferData, 0, bufferSize);
			}
			bos.flush();
		}

		private void readRequestData() throws IOException {
			List<String> lines = new ArrayList<>();
			// read request and headers
			String line = br.readLine();
			while (line != null && !line.equals("") && !line.equals(HTTP_NEW_LINE)) {
				lines.add(line);
				line = br.readLine();
			}
			
			// TODO: read body data, currently it doesn't read body data, for I don't know reasons
//			lines.add(HTTP_NEW_LINE);
//			line = br.readLine();
//			while(line != null && !line.equals("")) {
//				lines.add(line);
//				line = br.readLine();
//			}
			
			parsedRequest = Request.parse(lines);
			requestSocket.shutdownInput();
		}
	}

	/**
	 * Contains data about incoming HTTP request such as method, HTTP version, etc
	 * Used by methods that respond to the request
	 *
	 */
	private static class Request {

		private String method = "";
		private String path = "";
		private List<String[]> queryParams = new ArrayList<>();
		private String httpVersion = "";

		// note: currently not used field, kept for demonstration and later use
		private List<String> headers = new ArrayList<>();
		private List<String> data = new ArrayList<>();

		public static Request parse(List<String> lines) {
			Request parsedRequest = new Request();
			if (lines.isEmpty()) {
				return parsedRequest;
			}

			String[] firstLineParts = lines.get(0).split(" ");

			if (firstLineParts.length >= 1) {
				parsedRequest.method = firstLineParts[0];
			}

			if (firstLineParts.length >= 2) {
				String[] resourcePath = firstLineParts[1].split("\\?");
				parsedRequest.path = root + Paths.get(resourcePath[0]).toAbsolutePath().toString();
				if(resourcePath.length >= 2) {
					String[] params = resourcePath[1].split("&");
					for(String qp : params) {
						parsedRequest.queryParams.add(qp.split("="));
					}
				}
			}

			if (firstLineParts.length >= 3) {
				parsedRequest.httpVersion = firstLineParts[2];
			}
			
			boolean isData = false;
			for(int i = 0; i < lines.size(); i++) {
				if(!isData && lines.get(i).equals(HTTP_NEW_LINE)) {
					isData = true;
				}
				
				if(isData) {
					parsedRequest.data.add(lines.get(i));
				}
			}
			
			return parsedRequest;
		}

	}
}
