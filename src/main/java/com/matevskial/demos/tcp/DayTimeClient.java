package com.matevskial.demos.tcp;

import java.io.*;
import java.net.Socket;

/**
 * Represents the TCP DayTime client
 * Used for demonstrating TCP
 */
public class DayTimeClient {

    private final String host;
    private final int port;
    private final int waitForResponseMillis;

    public DayTimeClient(String host, int port, int waitForResponseMillis) {
        this.host = host;
        this.port = port;
        this.waitForResponseMillis = waitForResponseMillis;
    }

    public void requestForDayTimeData() throws IOException {
        Socket socket = new Socket(host, port);
        socket.setSoTimeout(waitForResponseMillis);

        InputStream socketInputStream = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(socketInputStream));
        String dayTime = br.readLine();
        System.out.println(dayTime);

        socket.close();
    }
}
