package com.matevskial.demos.tcp;

import java.io.*;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Represents the TCP DayTime server
 * Used for demonstrating TCP
 */
public class DayTimeServer implements Runnable {

    private final ServerSocket socket;

    public DayTimeServer(int port) throws IOException {
        socket = new ServerSocket(port);
    }

    @Override
    public void run() {
        while(true) {
            try {
                Socket response = socket.accept();
                respondWithCurrentDayTime(response);
            } catch (IOException e) {
                System.out.println("Error during listening for connections");
            }
        }

    }

    private void respondWithCurrentDayTime(Socket response) throws IOException {
        OutputStream responseOutputStream = response.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(responseOutputStream));

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        bw.write(dtf.format(now));
        bw.newLine();

        bw.close();
        response.close();
    }
}
