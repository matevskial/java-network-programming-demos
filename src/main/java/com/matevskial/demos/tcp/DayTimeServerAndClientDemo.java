package com.matevskial.demos.tcp;

import com.matevskial.demoframework.DemoMenu;
import com.matevskial.demoframework.IDemo;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.BindException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * This is a driver class to start the client and the daytime server
 */
public class DayTimeServerAndClientDemo implements IDemo {
    private DayTimeServer server;
    private DayTimeClient client;

    private int port;

    @Override
    public void run() {
        promptForInitialData();
        startServer();
        runDemo();

    }

    private void runDemo() {
        DemoMenu m = new DemoMenu("Choose action");
        m.addEntry("Request daytime data from daytime server", this::requestForDayTimeData);
        m.display();
    }

    private void requestForDayTimeData() {
        try {
            client.requestForDayTimeData();
        } catch (UnknownHostException e) {
            System.out.printf("Unable to resolve hostname while creating socket - %s%n", e);
        } catch (InterruptedIOException e) {
            System.out.printf("Timeout reached while waiting for data - %s%n", e);
        } catch (IOException e) {
            System.out.printf("Error during reading or writing data to socket stream - %s%n", e);
        }
    }

    private void startServer() {
        System.out.println("Starting echo server");
        Thread threadServer = new Thread(server);
        threadServer.start();
    }

    private void promptForInitialData() {
        promptForServerData();
        promptForClientData();
    }

    private void promptForClientData() {
        int millis;
        boolean inputReceived = false;
        Scanner in = new Scanner(System.in);
        while(!inputReceived) {
            try {
                System.out.println("How much milliseconds should the client wait for the response from the echo server?");
                millis = Integer.parseInt(in.nextLine());
                String host = "localhost";
                client = new DayTimeClient(host, port, millis);
                inputReceived = true;
                System.out.println("Created the client");
            } catch (NumberFormatException e) {
                System.out.println("Please type a valid number to specify the milliseconds.");
            }
        }
    }

    private void promptForServerData() {
        boolean inputReceived = false;
        Scanner in = new Scanner(System.in);
        while(!inputReceived) {
            try {
                System.out.println("Which port do you want the server to run on?");
                port = Integer.parseInt(in.nextLine());
                validatePort(port);
                server = new DayTimeServer(port);
                inputReceived = true;
                System.out.println("Created the server");
            } catch (NumberFormatException e) {
                System.out.println("Please type a valid number to specify a port.");
            } catch (IllegalArgumentException e) {
                System.out.println("Please specify port from range 1024-65535.");
            } catch (BindException e) {
                System.out.printf("Unable to use the specified port to create the echo server. Please specify another port - %s%n", e);
            } catch (IOException e) {
                System.out.printf("IO exception - %s%n", e);
            }
        }
    }

    private void validatePort(int port) throws IllegalArgumentException {
        if(port < 1024 || port > 65535) {
            throw new IllegalArgumentException();
        }
    }
}
