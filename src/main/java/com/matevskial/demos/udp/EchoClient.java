package com.matevskial.demos.udp;

import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 * Represents the UDP client
 * Used for demonstrating UDP
 */
public class EchoClient {
    private final DatagramSocket socket;
    private final DatagramPacket packet;

    public EchoClient(String serverHost, int serverPort, int waitForResponseMillis) throws SocketException, UnknownHostException {
        socket = new DatagramSocket();
        socket.setSoTimeout(waitForResponseMillis);

        packet = new DatagramPacket(new byte[EchoServerAndClientDemo.MAX_BUFFER_LENGTH], EchoServerAndClientDemo.MAX_BUFFER_LENGTH);
        packet.setAddress(InetAddress.getByName(serverHost));
        packet.setPort(serverPort);
    }

    public void sendAndReceive(String data) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        PrintWriter pout = new PrintWriter(bout);
        pout.println(data);
        pout.close();
        packet.setData(bout.toByteArray());

        try {
            socket.send(packet);
        } catch (IOException e) {
            System.out.printf("IO exception happened while trying to send a packet - %s%n", e);
        }

        try {
            socket.receive(packet);
            ByteArrayInputStream bin = new ByteArrayInputStream(packet.getData());
            Scanner in = new Scanner(bin);
            System.out.printf("Received back from echo server: %s%n", in.next());
        } catch (IOException e) {
            System.out.printf("Packet not received - %s%n", e);
        }
    }
}
