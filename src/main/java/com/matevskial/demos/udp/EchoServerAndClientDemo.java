package com.matevskial.demos.udp;

import com.matevskial.demoframework.DemoMenu;
import com.matevskial.demoframework.IDemo;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * This is a driver class to start the client and the echo server
 */
public class EchoServerAndClientDemo implements IDemo {

    private EchoServer server;
    private EchoClient client;

    private int port;

    // Enough for most UDP clients
    static final int MAX_BUFFER_LENGTH = 4096;

    @Override
    public void run() {
        promptForInitialData();
        startServer();
        runDemo();

    }

    private void runDemo() {
        DemoMenu m = new DemoMenu("Choose action");
        m.addEntry("Send data from client and receive back data from echo server", this::promptForDataToSend);
        m.display();
    }

    private void promptForDataToSend() {
        Scanner in = new Scanner(System.in);
        System.out.println("What text do you want to send and echo back from the server?");
        String data = in.next();
        client.sendAndReceive(data);
    }

    private void startServer() {
        System.out.println("Starting echo server");
        Thread threadServer = new Thread(server);
        threadServer.start();
    }

    private void promptForInitialData() {
        promptForServerData();
        promptForClientData();
    }

    private void promptForClientData() {
        int millis;
        boolean inputReceived = false;
        Scanner in = new Scanner(System.in);
        while(!inputReceived) {
            try {
                System.out.println("How much milliseconds should the client wait for the response from the echo server?");
                millis = Integer.parseInt(in.nextLine());
                String host = "localhost";
                client = new EchoClient(host, port, millis);
                inputReceived = true;
                System.out.println("Created the client");
            } catch (NumberFormatException e) {
                System.out.println("Please type a valid number to specify the milliseconds.");
            } catch (SocketException e) {
                System.out.printf("Unable to create the socket for the client - %s%n", e);
            } catch (UnknownHostException e) {
                System.out.printf("Unable to resolve hostname - %s%n", e);
            }
        }
    }

    private void promptForServerData() {
        boolean inputReceived = false;
        Scanner in = new Scanner(System.in);
        while(!inputReceived) {
            try {
                System.out.println("Which port do you want the server to run on?");
                port = Integer.parseInt(in.nextLine());
                validatePort(port);
                server = new EchoServer(port);
                inputReceived = true;
                System.out.println("Created the server");
            } catch (NumberFormatException e) {
                System.out.println("Please type a valid number to specify a port.");
            } catch (IllegalArgumentException e) {
                System.out.println("Please specify port from range 1024-65535.");
            } catch (SocketException e) {
                System.out.println("Unable to use the specified port to create the echo server. Please specify another port");
            }
        }
    }

    private void validatePort(int port) throws IllegalArgumentException {
        if(port < 1024 || port > 65535) {
            throw new IllegalArgumentException();
        }
    }
}
