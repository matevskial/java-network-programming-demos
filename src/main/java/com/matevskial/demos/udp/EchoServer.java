package com.matevskial.demos.udp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Represents the UDP Echo server
 * Used for demonstrating UDP
 */
public class EchoServer implements Runnable {
    private final DatagramSocket socket;
    private final DatagramPacket packet;

    public EchoServer(int port) throws SocketException {
        socket = new DatagramSocket(port);
        packet = new DatagramPacket(new byte[EchoServerAndClientDemo.MAX_BUFFER_LENGTH], EchoServerAndClientDemo.MAX_BUFFER_LENGTH);
    }

    @Override
    public void run() {
        while(true) {
            try {
                socket.receive(packet);
                ByteArrayInputStream bin = new ByteArrayInputStream(packet.getData());
                Scanner in = new Scanner(bin);
                System.out.printf("received: %s, sending it back!%n", in.next());
                socket.send(packet);
            } catch (IOException e) {
                System.out.printf("Server is unable to receive a data from clients - %s%n", e);
            }
        }
    }
}
