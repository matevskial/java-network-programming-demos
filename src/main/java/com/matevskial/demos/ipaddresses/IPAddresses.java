package com.matevskial.demos.ipaddresses;

import com.matevskial.demoframework.DemoMenu;
import com.matevskial.demoframework.IDemo;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * A class that contains some demos for the INetAddress class
 */
public class IPAddresses implements IDemo {
    private static final String HEADER_TEXT = "Java InetAddress demo";
    @Override
    public void run() {
        DemoMenu m = new DemoMenu(HEADER_TEXT);
        m.addEntry("Get local host IP address using the method getLocalHost", this::localHostIpAddress);
        m.addEntry("Get IP address of user-entered host(ex. duckduckgo.com) using the method getByName", this::ipAddressOfHost);
        m.display();
    }

    private void ipAddressOfHost() {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter hostname(ex. duckduckgo.com)");
        String hostName = in.nextLine();
        try {
            InetAddress hostAddress = InetAddress.getByName(hostName);
            System.out.printf("The IP Address of the host %s is %s%n", hostName, hostAddress.getHostAddress());
            System.out.println("Note that the method getByName makes request to DNS serves to resolve the IP address");
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
        }
    }

    private void localHostIpAddress() {
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            System.out.println(localhost.getHostAddress());
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
        }

    }
}
