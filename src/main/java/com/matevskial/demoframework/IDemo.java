package com.matevskial.demoframework;

/**
 * Custom interface to mark classes that are going to contain various demos
 * and that can be put as an demo entry in DemoMenu instances
 */
public interface IDemo extends Runnable {

}
