package com.matevskial.demoframework;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Represents utility that displays some menu entries and waits for user input
 * to select an action to run.
 *
 * Actions to run are added as IDemo objects
 * so selected demos can be run using the overridden method run()
 */
public class DemoMenu {
    private final String headerText;
    private final List<String> strings;
    private final List<IDemo> demos;

    public DemoMenu(String headerText) {
        strings = new ArrayList<>();
        demos = new ArrayList<>();
        this.headerText = headerText;
    }

    public void addEntry(String text, IDemo demo) {
        strings.add(text);
        demos.add(demo);
    }

    public void display() {
        displayHeader();
        Scanner in = new Scanner(System.in);
        boolean back = false;
        while(true) {
            displayEntries();
            String entryUserInput = in.nextLine();
            if("quit".equals(entryUserInput)) {
                break;
            } else if("back".equals(entryUserInput)) {
                back = true;
                break;
            }
            ParseResult parseResult = ParseResult.parse(entryUserInput);
            if(parseResult.isError()) {
                System.out.println(parseResult.getErrorMessage());
            } else {
                execute(parseResult.entryIndex);
            }

        }


        if(!back) {
            in.close();
            System.exit(0);
        }

    }

    private void execute(int entryIndex) {
        IDemo demo = demos.get(entryIndex);
        demo.run();
    }

    private void displayHeader() {
        System.out.println(headerText);
    }

    private void displayEntries() {
        for(int i = 0; i < strings.size(); i++) {
            System.out.printf("%d. %s%n", i + 1, strings.get(i));
        }
    }

    /**
     * Objects instances of this class represent the result of the parsed input
     * The result is either correctly parsed integer, or error
     * fields e and errorMessage will NOT be null when the result from parsing is error otherwise both will be null
     */
    private static class ParseResult {
        private final Exception e;
        private final String errorMessage;

        public int entryIndex;

        private ParseResult(int entryIndex) {
            this.entryIndex = entryIndex;
            e = null;
            errorMessage = null;
        }

        private ParseResult(Exception e, String errorMessage) {
            this.e = e;
            this.errorMessage = errorMessage;
        }

        public static ParseResult parse(String input) {
            try {
                int entryIndex = Integer.parseInt(input) - 1;
                return new ParseResult(entryIndex);
            } catch (NumberFormatException e) {
                return new ParseResult(e, null);
            }

        }

        public boolean isError() {
            return e != null || errorMessage != null;
        }

        public String getErrorMessage() {
            if(e != null) {
                return e.getMessage();
            }

            return Objects.requireNonNullElse(errorMessage, "An error occurred during parsing user input");

        }
    }
}
