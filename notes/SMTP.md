# SMTP - Simple Mail Transfer Protocol

* Protocol for sending emails

```
< 220 # first response code from server when client connects to it
HELO hostname # client sents its hostname to the server
< 250 # response code from server acknowledging the hostname
> MAIL FROM:<email@address> # client sends MAIL FROM 
< 250 # response code from server acknowledging the FROM email address
> RCPT TO:<email@address>
< 250 # response code from server acknowledging the RCPT TO email address
> DATA
< 354 # response code from server acknowledging that it can receive data, the response will be different if data is not acceptable
> Subject: subject text
> To: someemail@address
> From: someemail@address
>
> body text\nnew line of body text\n.
< 250 # response code from server acknowledging that the message is sent, the response will be different if message may not have been sent
< QUIT
```