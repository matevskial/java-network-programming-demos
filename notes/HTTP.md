# HTTP - HyperText Transfer Protocol

# HTTP/1.0

## Request syntax

* GET request

```http request
GET /resource-path/resource HTTP/1.0
```

* POST request

```http request
POST /endpoint/endpoint-resurce HTTP/1.0
Content-type: application/x-www-form-urlencoded
Content-length: 21
User-Agent: SomeBrowser/1.0

User=david&answer=yes
```

* HEAD request
```http request
HEAD /resource-path/resurce HTTP/1.0
```

## Response syntax

```http request
HTTP/1.0 200 OK
Content-Type: text/html
Content-Length: 166

Some Text
Data
```

## Request headers

* Content-type
* Content-length
* User-agent

## Server response headers

* Content-Type
* Content-Length
* Server

## Server response status codes

* Client error(4xx)
    * 400 - bad request
    * 404 - not found