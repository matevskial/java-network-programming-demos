# POP3 - Post Office Protocol

* Protocol for reading emails

```
< OK # first response of the server when the client connects to it
> USER username # client sends username
< OK # response of the server to confirm the data is accepted
> PASS
< OK
> STAT
< 5 10 # number of messages and maxsize
> RETR 3 # retreive the third message
< Subject: subject text\n
  To: someemail@address\n
  From: someemail@address\n

  body text\nnew line of body text\n.
```