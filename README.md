# Network programming examples and demos in Java

This repository contains:

* demos for IP addresses


* Simple Echo UDP Server - server that returns 


* Simple DayTime TCP Server - server that returns current daytime upon request


* some multi-threading code examples


* simple HTTP server
    * responds with files
    
    * generates an HTML table of files and folders if the requested resource is folder